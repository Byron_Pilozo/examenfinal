package facci.pm.cedeno_anayely.examenfinal.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import facci.pm.cedeno_anayely.examenfinal.Activity.Main2Activity;
import facci.pm.cedeno_anayely.examenfinal.Modelo.Profesores;
import facci.pm.cedeno_anayely.examenfinal.R;

public class AdaptadorProfesores extends RecyclerView.Adapter<AdaptadorProfesores.MyViewHolder>{

    private ArrayList<Profesores> arrayList;

    public AdaptadorProfesores(ArrayList<Profesores> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lista_profesores, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        final Profesores profesores = arrayList.get(i);
        myViewHolder.id.setText("ID: " + profesores.getId());
        myViewHolder.Cedula2.setText("CEDULA: " + profesores.getCedula());
        myViewHolder.Nombres.setText("NOMBRES: " + profesores.getNombre());
        myViewHolder.Apellidos.setText("APELLIDOS: " + profesores.getApellido());
        myViewHolder.Materia2.setText("MATERIA: " + profesores.getMateria());
        myViewHolder.Titulo2.setText("PROFESIÓN: " + profesores.getProfesion());
        myViewHolder.titular.setText("TITULAR: " + profesores.getTitular());
        Picasso.get().load(profesores.getImagen()).into(myViewHolder.foto);

        myViewHolder.view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, Main2Activity.class);
                intent.putExtra("id", profesores.getId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private View view1;
        private ImageView foto;
        private TextView id, Nombres, Apellidos, Cedula2, Materia2, Titulo2, titular;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view1 = itemView;

            id = (TextView)view1.findViewById(R.id.IdProf);
            Nombres = (TextView)view1.findViewById(R.id.TxtNombre);
            Apellidos = (TextView)view1.findViewById(R.id.TxtApellido);
            Cedula2 = (TextView)view1.findViewById(R.id.TxtCedula);
            Materia2 = (TextView)view1.findViewById(R.id.TxtMateria);
            Titulo2 = (TextView)view1.findViewById(R.id.TxtTitulo);
            titular = (TextView)view1.findViewById(R.id.TxtTitular);
            foto = (ImageView)view1.findViewById(R.id.IMGProff);


        }
    }
}
